#!/bin/sh
set -e

VERSION=$2
DIR=scala-${VERSION}
TAR=../scala_${VERSION}.orig.tar.xz

mkdir $DIR
tar -xf $3 --strip-components=1 -C $DIR
rm $3

# Replace the minified JavaScript files
rm $DIR/src/compiler/scala/tools/nsc/doc/html/resource/lib/jquery.js
rm $DIR/src/compiler/scala/tools/nsc/doc/html/resource/lib/jquery-ui.js
rm $DIR/src/compiler/scala/tools/nsc/doc/html/resource/lib/modernizr.custom.js
rm $DIR/src/compiler/scala/tools/nsc/doc/html/resource/lib/raphael-min.js
rm $DIR/src/compiler/scala/tools/nsc/doc/html/resource/lib/tools.tooltip.js

wget http://code.jquery.com/jquery-1.8.2.js -O $DIR/src/compiler/scala/tools/nsc/doc/html/resource/lib/jquery.js
wget http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.js -O $DIR/src/compiler/scala/tools/nsc/doc/html/resource/lib/jquery-ui.js
wget http://modernizr.com/downloads/modernizr-2.5.3.js -O $DIR/src/compiler/scala/tools/nsc/doc/html/resource/lib/modernizr.custom.js
wget https://raw.githubusercontent.com/DmitryBaranovskiy/raphael/v2.1.0/raphael.js -O $DIR/src/compiler/scala/tools/nsc/doc/html/resource/lib/raphael-min.js
wget https://raw.githubusercontent.com/hungryblank/octosquashes-pub/master/couch/src/js/01_tools.tooltip-1.1.3.js -O $DIR/src/compiler/scala/tools/nsc/doc/html/resource/lib/tools.tooltip.js

XZ_OPT=--best tar -c -v -J -f $TAR \
    --exclude 'lib/ant' \
    $DIR
rm -Rf $DIR
